import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { Container } from "react-bootstrap";

const StyledContainer = styled(Container)`
  margin-top: 1rem;
  margin-bottom: 1rem;

  iframe {
    min-height: 710px;
    background: white;
    max-width: 100%;
    width: calc(100% - 2px);
    border-radius: 3px;
    border: 1px solid rgb(219, 219, 219);
    box-shadow: none;
    display: block;
    margin: 0px 0px 12px;
    min-width: 326px;
    padding: 0px;
  }
`;

const InstagramPost = props => {
  if (!props.url) return null;

  let url = props.url.replace(/(^\w+:|^)\/\//, "//").replace(/\/$/, "");

  if (!url.endsWith("/embed")) {
    url = `${url}/embed`;
  }

  const id =
    props.id || url.replace("/embed", "").substr(url.lastIndexOf("/") + 1);

  return (
    <StyledContainer className={`component-${props.name}`}>
      <iframe
        className="instagram-media instagram-media-rendered"
        id={`instagram-${id}`}
        src={url}
        data-instgrm-payload-id={`instagram-media-payload-${id}`}
        allowtransparency="true"
        frameBorder="0"
        scrolling="no"
      />
    </StyledContainer>
  );
};

InstagramPost.propTypes = {
  /** Component id, to handle for data-instgrm-payload-id attribute */
  id: PropTypes.string,
  /** Instagram embed url */
  url: PropTypes.string,
  /** Component name */
  name: PropTypes.string,
};

export default InstagramPost;
