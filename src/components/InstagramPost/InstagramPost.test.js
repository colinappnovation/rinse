import InstagramPost from "./InstagramPost.js";

const minProps = {
  id: "test-123",
  url: "//www.instagram.com/p/tsxp1hhQTG/embed",
};

describe("<InstagramPost />", () => {
  it("should render without exploding", () => {
    const wrapper = shallow(<InstagramPost {...minProps} />);
    expect(wrapper.length).toEqual(1);
    expect(wrapper).toMatchSnapshot();
  });
});
