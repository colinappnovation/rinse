import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import ShowMore from "./ShowMore";

const StyledDiv = styled.div`
  .linkto {
    color: ${props => props.theme.primaryButtonColor};
  }
  a {
    color: ${props => props.theme.primaryButtonColor};
  }

  .read-more-link {
    div {
      margin: 1rem 0 2rem;
    }
    color: ${props => props.theme.primaryButtonColor};
  }
`;

class ReadMoreText extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      expanded: false,
    };
    this.handleExpand = this.handleExpand.bind(this);
  }

  handleExpand() {
    this.setState({
      expanded: !this.state.expanded,
    });
  }

  render() {
    const { expanded } = this.state;
    const { lines, markup, children } = this.props;

    if (!markup && !children) {
      return null;
    }

    return (
      <StyledDiv>
        <ShowMore lines={lines} anchorClass="read-more-link">
          {markup || children}
        </ShowMore>
      </StyledDiv>
    );
  }
}

ReadMoreText.propTypes = {
  markup: PropTypes.string,
  lines: PropTypes.number,
};

ReadMoreText.defaultProps = {
  lines: 5,
};

export default ReadMoreText;
