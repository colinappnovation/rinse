import React, { Component } from "react";
import PropTypes from "prop-types";
import TruncateMarkup from "react-truncate-markup";
import convert from "htmr";
import styled from "styled-components";
//
import { transformMarkup } from "../../utils/common";

const StyledDiv = styled.div`
  .truncated p:last-of-type {
    &:after {
      content: "\\2026";
    }
  }
`;

const more = (
  <div>
    Read more <i className="zmdi zmdi-long-arrow-down" />
  </div>
);

const less = (
  <div>
    Read less <i className="zmdi zmdi-long-arrow-up" />
  </div>
);

class ShowMore extends Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: false,
      truncated: false,
    };
    this.handleTruncate = this.handleTruncate.bind(this);
    this.afterTruncate = this.afterTruncate.bind(this);
  }

  handleTruncate(truncated) {
    if (truncated !== this.state.truncated) {
      this.setState({
        truncated,
      });
    }
  }

  toggleLines = event => {
    event.preventDefault();

    this.setState({
      expanded: !this.state.expanded,
    });
  };

  afterTruncate(truncated) {
    if (this.state.truncated !== truncated) {
      this.setState({ truncated });
    }
  }

  render() {
    const { children, more, less, lines, anchorClass } = this.props;
    const { expanded, truncated } = this.state;

    return (
      <StyledDiv>
        {!expanded && (
          <TruncateMarkup
            onAfterTruncate={this.afterTruncate}
            lines={lines}
            ellipsis={
              <span>
                <a href="#" className={anchorClass} onClick={this.toggleLines}>
                  {more}
                </a>
              </span>
            }
            onTruncate={this.handleTruncate}
          >
            <div className={truncated ? "truncated" : "full"}>
              {convert(children)}
            </div>
          </TruncateMarkup>
        )}

        {expanded && (
          <div>
            {convert(children, { transform: transformMarkup })}
            <span>
              {" "}
              <a href="#" className={anchorClass} onClick={this.toggleLines}>
                {less}
              </a>
            </span>
          </div>
        )}
      </StyledDiv>
    );
  }
}

ShowMore.propTypes = {
  children: PropTypes.node,
  lines: PropTypes.number,
  more: PropTypes.node,
  less: PropTypes.node,
  anchorClass: PropTypes.string,
};

ShowMore.defaultProps = {
  lines: 5,
  more: more,
  less: less,
  anchorClass: "",
};

export default ShowMore;
