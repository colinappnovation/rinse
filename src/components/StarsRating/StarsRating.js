import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

const Label = styled.span`
  margin-left: 10px;
`;

const StyledIcon = styled.i`
  color: blue;

  &.empty {
    color: #dedede;
  }
`;

const StarsRating = props => (
  <React.Fragment>
    {[1, 2, 3, 4, 5].map(star => {
      return (
        <span key={`star-${props.group}-${star}`}>
          <StyledIcon
            className={`zmdi zmdi-star ${star > props.stars && "empty"}`}
          />
        </span>
      );
    })}
    {props.label && <Label>{props.label}</Label>}
  </React.Fragment>
);

StarsRating.propTypes = {
  /** Group to aggrefate multiple rating elements */
  group: PropTypes.string.isRequired,
  /** Label of the stars element */
  label: PropTypes.string,
  /** Number of stars */
  stars: PropTypes.number,
};

export default StarsRating;
