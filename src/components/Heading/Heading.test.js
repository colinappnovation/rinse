import Heading from "./Heading.js";

const minProps = {
  level: "h1",
  text: "text",
};

describe("<Heading />", () => {
  it("should render without exploding", () => {
    const wrapper = shallow(<Heading {...minProps} />);
    expect(wrapper.length).toEqual(1);
    expect(wrapper).toMatchSnapshot();
  });
});
