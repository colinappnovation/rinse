import React from "react";
import PropTypes from "prop-types";

const Heading = props => {
  const { level, text, children } = props;
  const Tag = `${level}`;

  return (   
      <Tag>{text || children}</Tag>
  );
};

Heading.propTypes = {
  /** Selects the desired \<h\> element to use */
  level: PropTypes.oneOf(["h1", "h2", "h3", "h4", "h5", "h6"]).isRequired,
  /** Heading text (optional to children node) */
  text: PropTypes.string,
};

export default Heading;
