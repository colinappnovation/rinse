import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { Container, Figure } from "react-bootstrap";
//
import { getImagePath } from "../../utils/common";

const StyledContainer = styled(Container)`
  margin-bottom: 1rem;
  figure {
    margin-bottom: 1rem;
  }
`;

const StyledFigure = styled(Figure)`
  width: ${props => (props.fullwidth ? "100%" : "auto")};
  height: ${props => (props.fullwidth ? "100%" : "auto")};
  img {
    width: ${props => (props.fullwidth ? "100%" : "auto")};
    height: ${props => (props.fullwidth ? "100%" : "auto")};
  }
`;

const ImageBlock = props => {
  const { name, image, alt, caption, fullWidth, style } = props;

  if (!image) return null;

  const src = getImagePath(image, style || "Image710x400");

  return (
    <StyledContainer className={`component-${name}`}>
      <StyledFigure fullwidth={fullWidth ? 1 : 0}>
        <Figure.Image
          width="100%"
          alt={alt || image.description || image.title}
          title={alt || image.description || image.title}
          src={src}
        />
        {caption && <Figure.Caption>{caption}</Figure.Caption>}
      </StyledFigure>
    </StyledContainer>
  );
};

ImageBlock.propTypes = {
  /** Image alt attribute */
  alt: PropTypes.string,
  /** Caption text (optional to children node) */
  caption: PropTypes.string,
  /** Image props */
  image: PropTypes.shape({
    path: PropTypes.string.isRequired,
    _id: PropTypes.string,
    description: PropTypes.string,
    title: PropTypes.string,
    width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    styles: PropTypes.array,
  }).isRequired,
  /** Set image to full width (100%) */
  fullWidth: PropTypes.bool,
  /** An optional image style */
  style: PropTypes.string,
  /** Component name */
  name: PropTypes.string,
};

ImageBlock.defaultProps = {
  fullWidth: false,
  name: "ImageBlock",
};

export default ImageBlock;
