import ImageBlock from "./ImageBlock.js";

const cockpitImage = {
  _id: "5c36153d164bfc00b21d50d2",
  path: "https://via.placeholder.com/400x300/333",
  title: "Big Sky",
  alt: "Big Sky",
  styles: [
    {
      style: "card",
      path: "https://via.placeholder.com/250x250/333",
    },
  ],
};

describe("<ImageBlock />", () => {
  it("should render without exploding", () => {
    const wrapper = shallow(<ImageBlock image={cockpitImage} />);
    expect(wrapper.length).toEqual(1);
    expect(wrapper).toMatchSnapshot();
  });
});
