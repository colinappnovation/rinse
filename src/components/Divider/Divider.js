import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { Container } from "react-bootstrap";
//

const StyledContainer = styled(Container)`
  border-top: 1px solid
    ${props => props.theme[props.color] || props.theme.primaryColor};
  margin-bottom: 50px;
  margin-top: 50px;
`;

const Divider = props => (
  <StyledContainer className="divider" color={props.color} />
);

Divider.propTypes = {
  color: PropTypes.string,
};

Divider.defaultProps = {
  color: "primaryColor",
};

export default Divider;
