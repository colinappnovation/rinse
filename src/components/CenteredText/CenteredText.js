import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

const StyledDiv = styled.div`
  margin: 0px auto;
  max-width: 570px;
  text-align: center;
  margin-top: 0px;
  margin-bottom: 60px;
  font-size: 18px;
  padding-right: 20px;
  padding-left: 20px;
`;

const CenteredText = props => <StyledDiv>{props.text}</StyledDiv>;

CenteredText.propTypes = {
  text: PropTypes.string,
};

CenteredText.defaultProps = {
  text: "",
};

export default CenteredText;
