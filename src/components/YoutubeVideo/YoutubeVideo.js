import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { Container } from "react-bootstrap";
//
import { devices } from "../../utils/responsive";

const StyledContainer = styled(Container)`
  margin-top: 1rem;
  margin-bottom: 1rem;
  position: relative;
  height: 0;
  padding-bottom: ${props => props.height};

  iframe {
    position: absolute;
    top: 0;
    left: 0;
    padding-left: 0;
    padding-right: 0;
    width: 100%;
    height: 100%;
    max-height: ${props => props.height};
    @media (max-width: ${devices.mobileMax}px) {
      padding-left: 15px;
      padding-right: 15px;
    }
  }
`;

const YoutubeVideo = props => {
  if (!props.url) return null;

  const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
  const match = props.url.match(regExp);

  if (!match || match[2].length !== 11) {
    return null;
  }

  const videoId = match[2];

  const url = `//www.youtube.com/embed/${videoId}`;

  const controls = props.controls ? "1" : "0";
  const loop = props.loop ? `1&playlist=${videoId}` : "0";
  const autoplay = props.autoplay ? "1" : "0";
  const fs = props.fs ? "1" : "0";

  const src = `${url}?controls=${controls}&loop=${loop}&autoplay=${autoplay}&fs=${fs}&modestbranding=1`;

  return (
    <StyledContainer
      fluid={false}
      className={`component-${props.name}`}
      height={props.height || "390px"}
    >
      <iframe src={src} frameBorder="0" allowFullScreen={fs} />
    </StyledContainer>
  );
};

YoutubeVideo.propTypes = {
  /** Youtube video url */
  url: PropTypes.string,
  /** Height */
  height: PropTypes.string,
  /** Enable autoplay */
  autoplay: PropTypes.bool,
  /** Show controls */
  controls: PropTypes.bool,
  /** Show fullscreen button */
  fs: PropTypes.bool,
  /** Video loop option */
  loop: PropTypes.bool,
  /** Component name */
  name: PropTypes.string,
};

YoutubeVideo.defaultProps = {
  autoplay: false,
  controls: true,
  fs: true,
  loop: false,
  height: "390px",
};

export default YoutubeVideo;
