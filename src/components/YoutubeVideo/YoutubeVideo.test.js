import YoutubeVideo from "./YoutubeVideo.js";

const minProps = {
  url: "https://youtu.be/67iKigsSbQs",
};

describe("<YoutubeVideo />", () => {
  it("should render without exploding", () => {
    const wrapper = shallow(<YoutubeVideo {...minProps} />);
    expect(wrapper.length).toEqual(1);
    expect(wrapper).toMatchSnapshot();
  });
});
