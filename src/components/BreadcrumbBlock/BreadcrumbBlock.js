import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { Link } from "@reach/router";
import { Container, Breadcrumb } from "react-bootstrap";
//

const StyledContainer = styled(Container)`
  margin-top: 1rem;
  margin-bottom: 1rem;

  .breadcrumb {
    background-color: transparent;
  }
  .zmdi-home {
    font-size: 16px;
  }
  .breadcrumb-item {
    font-size: 12px;
  }
  .breadcrumb-item + .breadcrumb-item::before {
    content: ">";
    padding-right: 0.25em;
    padding-left: 0.25em;
  }
`;

const BreadcrumbBlock = props => {
  const { active, links } = props;
  const state = {};
  if (typeof window !== "undefined") {
    state.source = window.location.href;
  }

  return (
    <StyledContainer>
      <Breadcrumb>
        <li className="breadcrumb-item">
          <Link to="/" state={state}>
            <i className="zmdi zmdi-home" />
          </Link>
        </li>
        {links.map((link, idx) => (
          <li className="breadcrumb-item" key={`crumb-${idx}`}>
            <Link to={link.href} state={state}>
              {link.title}
            </Link>
          </li>
        ))}
        <Breadcrumb.Item active>{active}</Breadcrumb.Item>
      </Breadcrumb>
    </StyledContainer>
  );
};

BreadcrumbBlock.propTypes = {
  /** The active page title */
  active: PropTypes.string.isRequired,
  /** The links to display in the breadcrumb */
  links: PropTypes.array.isRequired,
};

export default BreadcrumbBlock;
