import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { Container, Row, Col } from "react-bootstrap";
//

const StyledContainer = styled(Container)`
  margin-top: 1rem;
  margin-bottom: 1rem;
  padding: 0;

  h3 {
    color: ${props => props.theme.primaryColor};
    font-size: 32px;
    font-weight: 500;
    margin-bottom: 35px;
  }

  .no-padding {
    padding: 0;
  }

  .gmap {
    min-height: ${props => props.height}px;
    max-height: 700px;
    background: white;
    max-width: 100%;
  }

  .gm-style-iw {
    max-width: 380px;
    h3 {
      font-size: 16px;
    }
  }
`;

class GoogleMap extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      map: null,
      isLoading: true,
    };
  }

  componentDidMount() {
    const { id, map } = this.props;

    if (!map) return null;

    const { lat, lng, marker, options } = map;
    if (google !== undefined && document !== undefined) {
      const gMap = new google.maps.Map(document.getElementById(id), {
        center: { lat: lat, lng: lng },
        zoom: parseInt(options.zoom) || 9,
        mapTypeId: options.type,
        disableDefaultUI: !options.controls,
      });
      if (options.marker) {
        const options = {
          draggable: false,
          position: { lat: lat, lng: lng },
          map: gMap,
        };
        if (marker && marker.title) {
          options.title = marker.title;
        }
        const gMapMarker = new google.maps.Marker(options);

        if (marker && (marker.title || marker.text)) {
          const infowindow = new google.maps.InfoWindow({
            content: `<h3>${marker.title || ""}</h3><div>${marker.text ||
              ""}</div>`,
          });
          gMapMarker.addListener("click", function() {
            infowindow.open(gMap, gMapMarker);
          });
        }
      }
    }
  }

  render() {
    const { map, id, name, title, fluid } = this.props;
    if (!map) return null;
    const { header, options } = map;
    const height = options.height || 400;
    return (
      <StyledContainer
        fluid={fluid}
        height={height}
        className={`component-${name}`}
      >
        <Container>
          <Row>
            <Col xs={12} className="no-padding">
              {(title || header) && <h3>{title || header}</h3>}
            </Col>
          </Row>
        </Container>
        <div id={id} className="gmap" />
      </StyledContainer>
    );
  }
}

GoogleMap.propTypes = {
  /** Component id */
  id: PropTypes.string,
  /** Component name */
  name: PropTypes.string,
  /** Optional title that overrides map header */
  title: PropTypes.string,
  /** Set map to be full width using fluid container (default) */
  fluid: PropTypes.bool,
  /** Map object */
  map: PropTypes.shape({
    /** Map Header */
    header: PropTypes.string,
    /** Map address */
    address: PropTypes.string,
    /** Map lat */
    lat: PropTypes.number.isRequired,
    /** Map lng */
    lng: PropTypes.number.isRequired,
    /** Marker (optional) */
    marker: PropTypes.shape({
      title: PropTypes.string,
      text: PropTypes.string,
    }),
    /** Options (optional) */
    options: PropTypes.shape({
      controls: PropTypes.bool,
      zoom: PropTypes.string,
      type: PropTypes.string,
      height: PropTypes.number,
      marker: PropTypes.bool,
    }),
  }),
};

GoogleMap.defaultProps = {
  fluid: true,
  options: {
    controls: true,
    zoom: "12",
    type: "roadmap",
    height: 400,
  },
};

export default GoogleMap;
