import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { Row, Col } from "react-bootstrap";
import { Link } from "@reach/router";
//
import { isExternalUrl } from "../../utils/common";

const List = styled.ul`
  padding: 0px;
  list-style-type: none;

  .icon-wrapper {
    margin-right: 5px;
    i {
      color: ${props => props.theme.primaryColorLight};
    }
  }
`;

const getFacilityLink = (link, text) => {
  if (isExternalUrl(link)) {
    return (
      <a href={link} target="_blank" rel="noopener noreferrer">
        {text}
      </a>
    );
  } else {
    return <Link to={link}>{text}</Link>;
  }
};

const CheckList = props => (
  <List>
    {props.items &&
      props.items.length > 0 &&
      props.items.map((item, idx) => (
        <React.Fragment key={`list-${idx}`}>
          {item && (
            <li>
              <Row noGutters>
                <Col xs={1} className="icon-wrapper">
                  <i className={`zmdi ${props.icon}`} />
                </Col>
                <Col xs={10}>
                  {(item.text &&
                    item.link &&
                    getFacilityLink(item.link, item.text)) ||
                    item.text ||
                    item}
                </Col>
              </Row>
            </li>
          )}
        </React.Fragment>
      ))}
  </List>
);

CheckList.propTypes = {
  /** The checklist items */
  items: PropTypes.array,
  icon: PropTypes.string,
};

CheckList.defaultProps = {
  items: [],
  icon: "zmdi-check",
};

export default CheckList;
