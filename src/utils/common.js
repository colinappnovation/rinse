export function isExternalUrl(url) {
    return /^(https?:\/\/|mailto:|tel:)/i.test(url)
}

export function getImagePath(image, style) {
    const IMAGE_PATH = process.env.ASSETS_URL;
  
    if (image === undefined || !image || image.path === undefined) {
      return null;
    }
  
    let url = image.path;
  
    if (style && image.styles !== undefined && image.styles.length) {
      const imageStyle = image.styles.find(styleObj => styleObj.style === style);
  
      if (imageStyle !== undefined) {
        url = imageStyle.path;
        if (isExternalUrl(url)) {
          return url;
        }
        if (!url.startsWith("/styles")) {
          url = `/styles${url}`;
        }
      }
    }
  
    if (isExternalUrl(url)) {
      return url;
    } else {
      return `${IMAGE_PATH}${url}`;
    }
  }