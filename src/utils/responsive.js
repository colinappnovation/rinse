export const devices = {
  smallMobileMin: 320,
  smallMobile: 414,
  mobileMax: 767.98,
  tabletMin: 768,
  tabletMax: 991.98,
  desktop: 992,
  desktopLarge: 1200,
  default: 768,
};
