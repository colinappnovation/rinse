import React from "react";
import { storiesOf } from "@storybook/react";
import { Container } from "react-bootstrap";
import { ImageBlock } from "../components";
import "../../.storybook/bootstrap.min.css";

const cockpitImage = {
  _id: "5c36153d164bfc00b21d50d2",
  path: "https://via.placeholder.com/400x300/333",
  title: "Big Sky",
  styles: [
    {
      style: "card",
      path: "https://via.placeholder.com/250x250/333",
    },
  ],
};

storiesOf("ImageBlock", module)
  .addParameters({
    info: "ImageBlock component that can be used in page containers.",
  })
  .addDecorator(story => <Container fluid>{story()}</Container>)
  .addWithJSX("default", () => (
    <ImageBlock
      image={{ path: "https://via.placeholder.com/700x400/333" }}
      alt="Example image alt"
    />
  ))
  .addWithJSX("with caption", () => (
    <ImageBlock
      image={{ path: "https://via.placeholder.com/550x350/333" }}
      alt="Example image alt"
      caption="A caption for the above image."
    />
  ))
  .addWithJSX("full width", () => (
    <ImageBlock
      image={{
        path: "https://via.placeholder.com/720x400/333?text=full+width+image",
      }}
      alt="Example image alt"
      fullWidth
    />
  ))
