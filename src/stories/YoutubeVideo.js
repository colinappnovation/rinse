import React from "react";
import { storiesOf } from "@storybook/react";
import { Container } from "react-bootstrap";
import { YoutubeVideo } from "../components";
import "../../.storybook/bootstrap.min.css";

storiesOf("YoutubeVideo", module)
  .addParameters({
    info: "Youtube Video component that can be used in page containers.",
  })
  .addDecorator(story => <Container>{story()}</Container>)
  .addWithJSX("default", () => <YoutubeVideo url="//youtu.be/DvnexzmShYk" />)
  .addWithJSX("without fullscreen", () => (
    <YoutubeVideo url="//youtu.be/DvnexzmShYk" fs={false} />
  ))
  .addWithJSX("with autoplay", () => (
    <YoutubeVideo url="//youtu.be/DvnexzmShYk" autoplay />
  ))
  .addWithJSX("without controls", () => (
    <YoutubeVideo url="//youtu.be/DvnexzmShYk" controls={false} />
  ))
  .addWithJSX("with loop", () => (
    <YoutubeVideo url="//youtu.be/DvnexzmShYk" loop />
  ))
  .addWithJSX("overriding default height (100%)", () => (
    <YoutubeVideo url="//youtu.be/DvnexzmShYk" height="100%" controls={false} />
  ))
  .addWithJSX("overriding default height (450px)", () => (
    <YoutubeVideo
      url="//youtu.be/DvnexzmShYk"
      height="450px"
      controls={false}
    />
  ));
