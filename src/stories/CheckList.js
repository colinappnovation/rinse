import React from "react";
import { storiesOf } from "@storybook/react";
import { Container, Col } from "react-bootstrap";
import { CheckList } from "../components";
import "../../.storybook/bootstrap.min.css";

const items1 = [
  "Nihilne te nocturnum praesidium Palati",
  "Nihil urbis vigiliae.",
  "Integer legentibus erat",
  "Ante historiarum dapibus.",
];

const items2 = [
  {
    text: "Nihilne te nocturnum praesidium Palati",
    link: "http://www.skisafari.com",
  },
  "Nihil urbis vigiliae.",
  { text: "Integer legentibus erat", link: "/contact-us" },
  "Ante historiarum dapibus.",
];

storiesOf("CheckList", module)
  .addParameters({
    info: "Small list of checked items",
  })
  .addDecorator(story => (
    <Container>
      <Col md={6}>{story()}</Col>
    </Container>
  ))
  .addWithJSX("default", () => <CheckList items={items1} />)
  .addWithJSX("items with links", () => <CheckList items={items2} />);
