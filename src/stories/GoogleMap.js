import React from "react";
import { storiesOf } from "@storybook/react";
import { Container } from "react-bootstrap";
import { GoogleMap } from "../components";
import "../../.storybook/bootstrap.min.css";

const mapProps1 = {
  header: "Brighton",
  address: "34 Blaker St, Brighton BN2 0GW, UK",
  lat: 50.82299443434174,
  lng: -0.13102016378593362,
  options: {
    controls: true,
    zoom: "11",
    type: "roadmap",
    height: 400,
    marker: false,
  },
};

const mapProps2 = {
  header: "Brighton",
  address: "34 Blaker St, Brighton BN2 0GW, UK",
  lat: 50.82299443434174,
  lng: -0.13102016378593362,
  options: {
    controls: false,
    zoom: "11",
    type: "roadmap",
    height: 400,
    marker: false,
  },
};

const mapProps3 = {
  header: "Brighton",
  address: "34 Blaker St, Brighton BN2 0GW, UK",
  lat: 50.82299443434174,
  lng: -0.13102016378593362,
  marker: {
    title: "Example",
    text:
      '<p>Morbi odio eros, <strong>volutpat</strong> ut pharetra vitae, lobortis sed nibh. At nos hinc posthac, sitientis piros Afros. Vivamus sagittis lacus vel augue laoreet rutrum faucibus. Ut enim ad minim veniam, quis nostrud exercitation. <em>Lorem ipsum</em> dolor sit amet, <em>consectetur</em> adipisici elit, sed eiusmod tempor incidunt ut labore et dolore <span style="text-decoration: underline;">magna aliqua</span>. Hi omnes lingua, institutis, legibus inter se differunt.</p>',
  },
  options: {
    controls: true,
    zoom: "11",
    type: "roadmap",
    height: 400,
    marker: true,
  },
};

const mapProps4 = {
  header: "Brighton",
  address: "34 Blaker St, Brighton BN2 0GW, UK",
  lat: 50.82299443434174,
  lng: -0.13102016378593362,
  options: {
    controls: true,
    zoom: "11",
    type: "satellite",
    height: 400,
    marker: false,
  },
};

storiesOf("GoogleMap", module)
  .addParameters({
    info: "Google Map component that can be used in page containers.",
  })
  .addDecorator(story => <Container fluid>{story()}</Container>)
  .addWithJSX("default (fluid)", () => <GoogleMap id="map1" map={mapProps1} />)
  .addWithJSX("default (no fluid)", () => (
    <GoogleMap id="map1" fluid={false} map={mapProps1} />
  ))
  .addWithJSX("without controls", () => <GoogleMap id="map2" map={mapProps2} />)
  .addWithJSX("with marker", () => <GoogleMap id="map3" map={mapProps3} />)
  .addWithJSX("satellite", () => <GoogleMap id="map4" map={mapProps4} />);
