import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import { Container, Col } from "react-bootstrap";
import { ReadMoreText } from "../components";
import "../../.storybook/bootstrap.min.css";

const markup = `<p><span>Ab illo tempore, ab est sed immemorabili.</span><span>Tu quoque, <a href="#">Brute</a>, fili mi, nihil timor populi, nihil!</span><span>Excepteur sint obcaecat cupiditat non proident culpa.</span> <span>Hi omnes lingua, institutis, legibus inter se differunt.</span> <span>Unam incolunt Belgae, aliam Aquitani, tertiam.</span></p><p><span>A communi observantia non est recedendum.</span> <span>Donec sed odio operae, eu vulputate felis rhoncus.</span> <span>Idque Caesaris facere voluntate liceret: sese habere.</span> <span>Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua.</span></p><p><span>Mercedem aut nummos unde unde extricat, amaras.</span> <span>Ambitioni dedisse scripsisse iudicaretur.</span> <span>Curabitur est gravida et libero vitae dictum.</span> <span>Me non paenitet nullum festiviorem excogitasse ad hoc.</span> <span>Integer legentibus erat a ante historiarum dapibus.</span></p><p><span>Hi omnes lingua, institutis, legibus inter se differunt.</span> <span>Morbi odio eros, volutpat ut pharetra vitae, lobortis sed nibh.</span> <span>Quis aute iure reprehenderit in voluptate velit esse.</span> <span>Unam incolunt Belgae, aliam Aquitani, tertiam.</span> <span>Me non paenitet nullum festiviorem excogitasse ad hoc.</span></p>`;

storiesOf("ReadMoreText", module)
  .addParameters({
    info: "markup with read more/less action",
  })
  .addDecorator(story => (
    <Container>
      <Col md={6}>{story()}</Col>
    </Container>
  ))
  .addWithJSX("default (5 lines)", () => (
    <ReadMoreText lines={5} markup={markup} />
  ))
  .addWithJSX("default (10 lines)", () => (
    <ReadMoreText lines={10} markup={markup} />
  ));
