import React from "react";
import { storiesOf } from "@storybook/react";
import { Container } from "react-bootstrap";
import { Heading } from "../components";
import "../../.storybook/bootstrap.min.css";

storiesOf("Heading", module)
  .addParameters({
    info:
      "This is an informational paragraph you can use to describe your component",
  })
  .addDecorator(story => <Container>{story()}</Container>)
  .addWithJSX("h1", () => <Heading level="h1" text="H1 Heading" />)
  .addWithJSX("h2", () => <Heading level="h2" text="H2 Heading" />)
  .addWithJSX("h3", () => <Heading level="h3" text="H3 Heading" />)
  .addWithJSX("h4", () => <Heading level="h4" text="H4 Heading" />)
  .addWithJSX("h5", () => <Heading level="h5" text="H5 Heading" />)
  .addWithJSX("h6", () => <Heading level="h6" text="H6 Heading" />)
