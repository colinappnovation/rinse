import React from "react";
import { storiesOf } from "@storybook/react";
import { Container } from "react-bootstrap";
import { BreadcrumbBlock } from "../components";
import "../../.storybook/bootstrap.min.css";

const links1 = [{ href: "/blog", title: "Blog" }];
const links2 = [
  { href: "/blog", title: "Blog" },
  { href: "/blog/news", title: "News" },
];

storiesOf("BreadcrumbBlock", module)
  .addParameters({
    info: "Breadcrumb to be used on containers",
  })
  .addDecorator(story => <Container>{story()}</Container>)
  .addWithJSX("2 levels", () => (
    <BreadcrumbBlock active="post 1" links={links1} />
  ))
  .addWithJSX("3 levels", () => (
    <BreadcrumbBlock active="post 2" links={links2} />
  ));
