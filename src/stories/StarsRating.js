import React from "react";
import { storiesOf } from "@storybook/react";
import { Container, Col } from "react-bootstrap";
import { StarsRating } from "../components";
import "../../.storybook/bootstrap.min.css";

storiesOf("StarsRating", module)
  .addParameters({
    info: "Star based rating with label",
  })
  .addDecorator(story => (
    <Container>
      <Col md={4}>{story()}</Col>
    </Container>
  ))
  .addWithJSX("default (1 star)", () => (
    <StarsRating group="classification" label="Beginner" stars={1} />
  ))
  .addWithJSX("default (5 stars)", () => (
    <StarsRating group="classification" label="Intermediate" stars={5} />
  ))
  .addWithJSX("default (3 stars)", () => (
    <StarsRating group="classification" label="Advanced" stars={3} />
  ));
