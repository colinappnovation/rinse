import React from "react";
import { storiesOf } from "@storybook/react";
import { CenteredText } from "../components";
import "../../.storybook/bootstrap.min.css";

const text =
  "Quam temere in vitiis, legem sancimus haerentia. Cum ceteris in veneratione tui montes, nascetur mus. Curabitur blandit tempus ardua ridiculus sed magna.";

storiesOf("CenteredText", module)
  .addParameters({
    info: "CenteredText component",
  })
  .addWithJSX("default", () => <CenteredText text={text} />);
