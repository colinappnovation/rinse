import React from "react";
import { storiesOf } from "@storybook/react";
import { Container } from "react-bootstrap";
import { InstagramPost } from "../components";
import "../../.storybook/bootstrap.min.css";

storiesOf("InstagramPost", module)
  .addParameters({
    info: "Instagram Post component that can be used in page containers.",
  })
  .addDecorator(story => <Container>{story()}</Container>)
  .addWithJSX("default", () => (
    <InstagramPost id="test-1" url="//www.instagram.com/p/tsxp1hhQTG" />
  ));
