import React from "react";
import { storiesOf } from "@storybook/react";
import { Divider } from "../components";
import "../../.storybook/bootstrap.min.css";

storiesOf("Divider", module)
  .addParameters({
    info: "Divider component",
  })
  .addWithJSX("default", () => <Divider />)
  .addWithJSX("lightGrey color", () => <Divider color="lightGrey" />);
