import { setAddon, configure } from '@storybook/react';
import JSXAddon from "storybook-addon-jsx";
const req = require.context('../src/stories/', true, /\.js?$/);

setAddon(JSXAddon);

function loadStories() {
  req.keys().forEach((filename) => req(filename))
}

configure(loadStories, module);
